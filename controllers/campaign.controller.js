'use strict';

const facebookService = require('../services/facebook.service');

class CampaignController {
    constructor(server) {
        server.get('campaign/:id', this.getCampaignsByAccountId.bind(this));
        server.get('campaigns/:id/adsets', this.getAdSets.bind(this));
        server.get('campaigns/:id/ads', this.getAds.bind(this));
        server.get('campaigns/:id/creatives', this.getCreativesByAccountId.bind(this));
        server.get('post/:postId/validIfInstagramElegible', this.validIfInstagramElegible.bind(this));
        server.get('account/:id/instagrams', this.getInstagramAccounts.bind(this));
        server.get('account/:id/customaudience/:accessToken', this.getSavedAudience.bind(this));
        server.post('campaigns/:id/create', this.createCampaign.bind(this));
        server.post('campaigns/:id/ad/create', this.createAd.bind(this));
        server.post('campaigns/:id/adsets/create', this.createAdSet.bind(this));
        server.post('creatives/:id/create/instagram', this.createCreativeForInstagram.bind(this));
        server.post('creatives/:id/create', this.createCreative.bind(this));
    }

    // Campaign Methods
    async getCampaignsByAccountId(req, res, next) {
        await facebookService
            .getCampaignsByAccountId(req.params.id)
            .then(campaigns => res.send(campaigns))
            .catch(err => res.status(500).send(err));
    }

    async createCampaign(req, res, next) {
        await facebookService
            .createCampaign(req.params.id, req.body)
            .then(createdCampaign => res.send(createdCampaign.id))
            .catch(err => res.status(500).send(`Comunicar al encargado del proyecto el siguiente error: ${err}`));
    }

    async getSavedAudience(req, res, next) {
        await facebookService
            .getSavedAudience(req.params.id, req.params.accessToken)
            .then(savedAudience => res.send(savedAudience))
            .catch(err => res.status(500).send(err));
    }

    async getAdSets(req, res, next) {
        await facebookService
            .getAdSets(req.params.id)
            .then(adSets => res.send(adSets))
            .catch(err => res.status(500).send(err));
    }

    async createAd(req, res, next) {
        await facebookService
            .createAd(req.params.id, req.body)
            .then(response => {
                return res.send(response);
            })
            .catch(err => {
                return res.status(500).send(err);
            });
    }

    async getAdSetsByCampaign(req, res, next) {
        await facebookService
            .getAdSetsByCampaign(req.params.id, rep.params.campaignId)
            .then(adSets => res.send(adSets))
            .catch(err => res.status(500).send(err));
    }

    async createAdSet(req, res, next) {
        await facebookService
            .createAdSet(req.params.id, req.body)
            .then(response => res.send(response.id))
            .catch(err => res.status(500).send(err));
    }

    async getCreativesByAccountId(req, res, next) {
        await facebookService
            .getCreatives(req.params.id)
            .then(creatives => res.send(creatives))
            .catch(err => res.status(500).send(err));
    }

    async createCreative(req, res, next) {
        await facebookService
            .createCreative(req.params.id, req.body)
            .then(creativeId => res.send(creativeId))
            .catch(err => res.status(500).send(err));
    }

    async getInstagramAccounts(req, res, next) {
        await facebookService
            .getInstagramAccounts(req.params.id)
            .then(igAccounts => res.send(igAccounts))
            .catch(err => res.status(500).send(err));
    }

    async createCreativeForInstagram(req, res, next) {
        await facebookService
            .createCreative(res.params.id, res.body)
            .then(creativeId => res.send(creativeId))
            .catch(err => res.status(500).send(err));
    }

    async validIfInstagramElegible(req, res, next) {
        await facebookService
            .validIfInstagramElegible(req.params.postId)
            .then(response => res.send(response))
            .catch(err => res.status(500).send(err));
    }

    async getAds(req, res, next) {
        await facebookService
            .getAds(req.params.id)
            .then(ads => res.send(ads))
            .catch(err => res.status(500).send(err));
    }

    async getAdsByAdSetId(req, res, next) {
        await facebookService
            .getAdsByAdSetId(req.params.id, req.params.adSetId)
            .then(ads => res.send(ads))
            .catch(err => res.status(500).send(err));
    }

    async getSavedAudience(req, res, next) {
        await facebookService
            .getSavedAudience(req.params.id, req.params.accessToken)
            .then(savedAudience => res.send(savedAudience))
            .catch(err => res.status(500).send(err));
    }
}

module.exports = CampaignController;
