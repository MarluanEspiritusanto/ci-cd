const facebookService = require('../services/facebook.service');

class PostController {
    constructor(server) {
        server.get('post/:pageId/posts_schedules/:accessToken', this.getPostSchedules.bind(this));
        server.get('post/:pageId/posts_promotional/:accessToken', this.getPromotionalPosts.bind(this));
    }

    async getPosts(req, res, next) {
        await facebookService
            .getPagePosts(req.params.pageId, req.params.accessToken, req.params.limit)
            .then(posts => res.send(posts))
            .catch(err => res.status(500).send(err));
    }

    async getPostSchedules(req, res, next) {
        await facebookService
            .getSchedulePosts(req.params.pageId, req.params.accessToken, 10)
            .then(schedulePosts => res.send(schedulePosts))
            .catch(err => res.status(500).send(err));
    }

    async getPromotionalPosts(req, res, next) {
        await facebookService
            .getPromotablePosts(req.params.pageId, req.params.accessToken, 10)
            .then(promotablePosts => res.send(promotablePosts))
            .catch(err => res.status(500).send(err));
    }
}

module.exports = PostController;
