const facebookService = require('../services/facebook.service');
const AdSet = require('../models/ad-set.model');
const Creative = require('../models/creative.model');
const Ad = require('../models/ad.model');
const Post = require('../models/post.model');
const Campaign = require('../models/campaign.model');
const ACCOUNT_STATUS = require('../helpers/account-status');
const Logger = require('../helpers/logger');

//TODO: Arreglar Logger

class PostItemController {
    constructor(server) {
        server.post('item/:pageId/:accessToken/:accountId', this.createSchedulePost.bind(this));
        server.post('post/autocampaign', this.executeAutocampaign.bind(this));
        server.get('instagram/:accountId', this.getInstagramAccounts.bind(this));
    }

    async getInstagramAccounts(req, res, next) {
        await facebookService
            .getInstagramAccounts(`act_${req.params.accountId}`)
            .then(response => {
                console.log(response);
            })
            .catch(err => {
                console.log(err);
            });
    }

    async executeAutocampaign(req, res, next) {
        let conditions = !req.body.schedulePosts || !req.body.pageId || !req.body.accountId || !req.body.countries,
            accessToken = null,
            reviewerEmail = req.body.reviewerEmail || 'Unspecified email',
            response = [];

        if (conditions) {
            const warningMessage = 'AccountId || PageId || Countries || SchedulePosts[] must be sents';
            Logger.warning(warningMessage);
            return res.status(400).send({ ok: false, error: warningMessage });
        }

        let schedulePosts = req.body.schedulePosts.map(schedulePost => {
            return new Post(schedulePost);
        });

        await this.getTokenByPageId(req.body.pageId)
            .then(token => {
                accessToken = token;
            })
            .catch(err => {
                Logger.error(new Error(`Access token for { ${req.body.pageId} } not found`));
            });

        if (!accessToken) {
            return res.status(500).send({
                ok: false,
                error: 'Error trying to get access token. Check the PageId'
            });
        }

        let IDS = {
            campaignId: null,
            adSetId: null,
            creativeId: null,
            adId: null,
            facebookPostId: null,
            instagramPostId: null,
            pageId: req.body.pageId,
            accountId: `act_${req.body.accountId}`
        };

        for (const schedulePost of schedulePosts) {
            await facebookService
                .createScheduledPost(IDS.pageId, schedulePost, accessToken)
                .then(createdPostId => {
                    if (!createdPostId) {
                        Logger.error(new Error('PostId not found'));
                        return;
                    }

                    Logger.info(`Scheduled post { ${createdPostId.id} } was created`);
                    IDS.facebookPostId = `${IDS.pageId}_${createdPostId.id}`;
                    let campaing = new Campaign({
                        name: schedulePost.name,
                        spendCap: schedulePost.spend_cap,
                        status: ACCOUNT_STATUS.Active
                    });

                    return facebookService.createCampaign(IDS.accountId, campaing);
                })
                .then(campaignId => {
                    if (!campaignId) {
                        Logger.error(new Error('CampaignId not found'));
                        return;
                    }

                    Logger.info(`Campaign { ${campaignId.id} } was created`);
                    campaignId = campaignId.id;
                    IDS.campaignId = campaignId;

                    let adSet = new AdSet();
                    adSet.name = schedulePost.name;
                    adSet.campaignId = campaignId;
                    adSet.status = ACCOUNT_STATUS.Active;
                    adSet.countries = req.body.countries;
                    adSet.startTime = schedulePost.scheduled_publish_time;
                    adSet.dailyBudget = schedulePost.daily_budget;
                    adSet.endTime = schedulePost.schedule_end_time;

                    return facebookService.createAdSet(IDS.accountId, adSet);
                })
                .then(adSetId => {
                    if (!adSetId) {
                        Logger.error(new Error('AdSetId not found'));
                        return;
                    }
                    Logger.info(`Adset { ${adSetId.id} } was created`);
                    adSetId = adSetId.id;
                    IDS.adSetId = adSetId;

                    let creative = new Creative();
                    creative.status = ACCOUNT_STATUS.Active;
                    creative.objectStoryId = IDS.facebookPostId;
                    creative.name = IDS.facebookPostId;

                    return facebookService.createCreative(IDS.accountId, creative);
                })
                .then(creativeId => {
                    if (!creativeId) {
                        Logger.error(new Error('CreativeId not found'));
                        return;
                    }

                    Logger.info(`Creative { ${creativeId} } was created`);
                    IDS.creativeId = creativeId;

                    let ad = new Ad();
                    ad.name = schedulePost.name;
                    ad.status = ACCOUNT_STATUS.Active;
                    ad.adSetId = IDS.adSetId;
                    ad.creativeId = creativeId;

                    return facebookService.createAd(IDS.accountId, ad);
                })
                .then(adId => {
                    if (!adId) {
                        Logger.error(new Error('AdId not found'));
                        return;
                    }

                    Logger.info(`Ad { ${adId} } was created`);
                    IDS.adId = adId;
                    schedulePost.errors = null;
                    schedulePost.ids = IDS;
                    schedulePost.reviewerEmail = reviewerEmail;
                    response.push(schedulePost);
                    console.log('################################');
                })
                .catch(err => {
                    Logger.error(new Error(`An error has occurred trying to create schedule post. AccountId: ${IDS.accountId} || PageId: ${IDS.pageId}`));
                    schedulePost.errors = err;
                    schedulePost.ids = IDS;
                    schedulePost.reviewerEmail = reviewerEmail;
                    response.push(schedulePost);
                    console.log('################################');
                });
        }

        Logger.info(`Post and campaign creation has finished`);
        return res.send(response);
    }

    getTokenByPageId(pageId) {
        return facebookService.getTokenByPageId(pageId);
    }

    createSchedulePost(req, res, next) {
        if (!req.body) {
            return res.status(500).send({
                ok: false,
                error: 'Server error'
            });
        }

        req.body.map(post => {
            let campaign = {
                account_id: post.accountId,
                name: post.campaign_name,
                spend_cap: post.SpendCap,
                start_time: post.scheduled_publish_time,
                end_time: post.end_time,
                daily_budget: post.daily_budget,
                countries: post.countries,
                object_story_id: '',
                message: post.message,
                creative_id: '',
                campaign_id: '',
                ad_set_id: '',
                ad_id: ''
            };
            this.createPromotedCampaign(campaign);
        });
    }

    //TODO Resolver problema con anidacion de promesas
    async createPromotedCreative(campaign) {
        await facebookService
            .getCreativesByAccountId(campaign.accountId)
            .then(response => {
                response.forEach(creative => {
                    if (creative.body === campaign.message) {
                        campaign.creativeId = creative.id;
                    }
                });

                if (post.creativeId === '') {
                    facebookService
                        .createCampaign(campaign.accountId, {
                            object_story_id: campaign.object_story_id,
                            name: 'Creative_' + campaign.name,
                            status: 'paused'
                        })
                        .then(response => {
                            let creativeId = response.id;
                            campaign.creativeId = creativeId;
                            this.createPromotedCampaign(campaign);
                        });
                } else {
                    this.createPromotedCampaign(campaign);
                }
            })
            .catch(err => {});
    }

    async createPromotedAdSet(campaign) {
        facebookService
            .getAdSetsByCampaign(campaign.accountId, campaign.campaignId)
            .then(response => {
                response.forEach(AdSet => {
                    if (AdSet.name === campaign.name) {
                        campaign.adSetId = AdSet.id;
                        this.createPromotedAds(campaign);
                    }
                });

                if (post.adSetId === '') {
                    facebookService
                        .createAdSet(campaign.accountId, {
                            name: post.name,
                            campaign_id: post.campaignId,
                            start_time: post.start_time,
                            end_time: post.end_time,
                            daily_budget: post.daily_budget,
                            countries: post.countries
                        })
                        .then(adSetResponse => {
                            post.adSetId = adSetResponse.id;
                            this.createPromotedAds(campaign);
                        });
                }
            })
            .catch(err => {});
    }

    async createPromotedAds(campaign) {
        await facebookService
            .getAdsByAdSetId(campaign.accountId, campaign.adSetId)
            .then(response => {
                response.forEach(ads => {
                    if (campaign.name === ads.name) {
                        campaign.adId = AdSet.id;
                    }
                });

                if (campaign.adId === '') {
                    facebookService
                        .createAd(campaign.accountId, {
                            name: campaign.name,
                            adSetId: campaign.adSetId,
                            creativeId: campaign.creativeId
                        })
                        .then(adResponse => {
                            campaign.adId = adResponse.id;
                        });
                }
            })
            .catch(err => {});
    }

    async createPromotedCampaign(campaign) {
        await facebookService.getCampaignsByAccountId(campaign.accountId).then(response => {
            response.forEach(_campaign => {
                if (_campaign.name === post.name) {
                    post.campaignId = _campaign.id;
                    this.cre;
                    this.createPromotedAdSet(campaign);
                }
            });
            if (campaign.campaignId === '') {
                facebookService.createCampaign(campaign.accountId, { name: campaign.name, SpendCap: campaign.SpendCap }).then(_campaign => {
                    campaign.campaignId = _campaign.id;
                    this.createPromotedAdSet(campaign);
                });
            }
        });
    }
}

module.exports = PostItemController;
