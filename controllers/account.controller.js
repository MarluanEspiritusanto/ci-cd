const facebookService = require('../services/facebook.service');

class AccountController {
    constructor(server) {
        server.get('account/:limit', this.getAccounts.bind(this));
    }

    async getAccounts(req, res, next) {
        return await facebookService.getAllAccounts(req.params.limit).then(accounts => res.send(accounts));
    }
}

module.exports = AccountController;
