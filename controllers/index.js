const AccountController = require('./account.controller');
const PostController = require('./post.controller');
const PostItemController = require('./post-item.controller');
const CampaignController = require('./campaign.controller');

module.exports = [AccountController, PostController, PostItemController, CampaignController];
