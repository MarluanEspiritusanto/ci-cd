'use strict';

class Post {
    constructor(post) {
        this.caption = post.caption;
        this.published = post.published || false;
        this.scheduled_publish_time = post.scheduledPublishTime;
        this.schedule_end_time = post.scheduledEndTime;
        this.spend_cap = post.spendCap;
        this.daily_budget = post.dailyBudget;
        this.name = post.name;
        // Image URL
        this.url = post.url;
    }
}

module.exports = Post;
