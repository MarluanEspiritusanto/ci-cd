'use strict';

class AdSet {
    constructor() {
        this.name = '';
        this.campaignId = '';
        this.startTime = '';
        this.endTime = '';
        this.dailyBudget = '';
        this.status = '';
        this.countries = '';
    }
}

module.exports = AdSet;
