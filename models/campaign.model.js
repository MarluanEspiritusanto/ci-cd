'use strict';

class Campaign {
    constructor(campaign) {
        this.spendCap = campaign.spendCap;
        this.name = campaign.name;
        this.status = campaign.status || 'INACTIVE';
    }
}

module.exports = Campaign;
