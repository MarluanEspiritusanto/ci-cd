'use strict';

const corsMD = require('../middlewares/cors.middleware');
const bodyParser = require('body-parser');

module.exports = function(app) {
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(corsMD);
};
