'use strict';

const express = require('express');
const app = express();
const BaseServer = require('./base-server');

class Server extends BaseServer {
    constructor() {
        super(app);
        this.app = app;
    }

    start(port) {
        try {
            // Middleware setup
            require('./middlewares')(this.app);

            // Controllers injection
            this.addControllers();

            return this.app.listen(port, () => {
                console.log(`Facebook Ads API up & running on port ${port}`);
            });
        } catch (ex) {
            console.log('Application has not be able to init. Error: ', ex.message);
        }
    }
}

module.exports = Server;
