'use strict';

const logger = require('../helpers/logger');

// Controllers import
const CONTROLLERS = require('../controllers');

class BaseServer {
    constructor(app) {
        this.app = app;
    }

    get(url, requestHandler) {
        this.addRoute('get', url, requestHandler);
    }

    post(url, requestHandler) {
        this.addRoute('post', url, requestHandler);
    }

    delete(url, requestHandler) {
        this.addRoute('delete', url, requestHandler);
    }

    put(url, requestHandler) {
        this.addRoute('put', url, requestHandler);
    }

    addRoute(httpMethod, url, requestHandler) {
        this.app[httpMethod](`/api/${process.env.APPLICATION_VERSION}/${url}`, async (req, res, next) => {
            try {
                await requestHandler(req, res, next);
            } catch (ex) {
                logger.error(ex);
                console.log(ex);
                ``;
                return res.status(500).send({ ok: false, message: 'Something bad has happened' });
            }
        });
    }

    addControllers() {
        CONTROLLERS.forEach(controller => new controller(this));
    }
}

module.exports = BaseServer;
