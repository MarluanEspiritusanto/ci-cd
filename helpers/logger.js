const bunyan = require('bunyan');
const seq = require('bunyan-seq');
const winston = require('winston');

class Logger {
    constructor() {
        this.log = bunyan.createLogger({
            name: 'Facebook Autocampaign API',
            streams: [
                {
                    stream: process.stdout,
                    level: 'warn'
                },
                seq.createStream({
                    serverUrl: process.env.SEQ_LOGGER_URL,
                    level: 'info'
                })
            ]
        });

        winston.add(new winston.transports.File({ filename: 'exceptions.log' }));
    }

    info(info) {
        this.log.info(`[(Facebook Autocampaign API)] — ${info}`);
        winston.info(info);
        console.log(info);
    }

    error(err) {
        this.log.error(`[(Facebook Autocampaign API)] — ${err.message}`);
        winston.error(err.message, err);
        console.log(err);
    }

    warning(warn) {
        this.log.warn({ lang: 'en' }, `[(Facebook Autocampaign API)] — ${warn}`);
        winston.warn(warn);
        console.log(warn);
    }
}

module.exports = new Logger();
