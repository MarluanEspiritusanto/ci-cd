'use strict';

module.exports = {
    Active: 'ACTIVE',
    Paused: 'PAUSED',
    Normal: 'NORMAL',
    Settings: 'SETTINGS',
    Duplicated: 'DUPLICATED',
    Inactive: 'INACTIVE'
};
