'use strict';

// Set enviroment variables
require('dotenv').config();

const Server = require('./startup/server');

// Run server
const server = new Server();
const serverOn = server.start(process.env.PORT || 8000);
module.exports = serverOn;
