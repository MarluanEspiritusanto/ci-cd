'use strict';

const axios = require('axios').default;
const AdAccount = require('facebook-nodejs-ads-sdk').AdAccount;
const Campaign = require('facebook-nodejs-ads-sdk').Campaign;
const AdSet = require('facebook-nodejs-ads-sdk').AdSet;
const Ad = require('facebook-nodejs-ads-sdk').Ad;
const AdCreative = require('facebook-nodejs-ads-sdk').AdCreative;
const Targeting = require('facebook-nodejs-ads-sdk').Targeting;
const CustomAudiences = require('facebook-nodejs-ads-sdk').CustomAudience;
const FacebookAdsApi = require('facebook-nodejs-ads-sdk').FacebookAdsApi;

class FacebookService {
    constructor() {
        FacebookAdsApi.init(process.env.FACEBOOK_API_ADS_TOKEN);
    }

    getAllAccounts(limit) {
        return axios.get(`${process.env.FACEBOOK_API_ADS_URL}me/accounts?limit=${limit}&access_token=${process.env.FACEBOOK_API_ADS_TOKEN}`).then(accounts => accounts.data);
    }

    getSchedulePosts(pageId, accessToken, limit) {
        let fields = 'id,object_id,scheduled_publish_time,message,picture,permalink_url,is_published,created_time,full_picture';
        return axios
            .get(`${process.env.FACEBOOK_API_ADS_URL + pageId}/scheduled_posts?fields=${fields}&is_published=false&limit=${limit}&access_token=${accessToken}`)
            .then(schedulePosts => schedulePosts.data);
    }

    getPagePosts(pageId, accessToken, limit) {
        let fields = 'id,object_id,scheduled_publish_time,message,picture,permalink_url,is_published,created_time,full_picture';
        return axios.get(`${process.env.FACEBOOK_API_ADS_URL + pageId}/posts?fields=${fields}&limit=${limit}&access_token=${accessToken}`).then(posts => posts.data);
    }

    getPromotablePosts(pageId, accessToken, limit) {
        let fields = 'id,object_id,scheduled_publish_time,message,picture,permalink_url,is_published,created_time,full_picture';
        return axios
            .get(`${process.env.FACEBOOK_API_ADS_URL + pageId}/promotable_posts?fields=${fields}&limit=${limit}&access_token=${accessToken}`)
            .then(promotablePosts => promotablePosts.data)
            .catch(err => {
                console.log(err);
                return 0;
            });
    }

    createScheduledPost(pageId, post, accessToken) {
        return axios.post(`${process.env.FACEBOOK_API_ADS_URL + pageId}/photos?&access_token=${accessToken}`, post).then(createdPost => createdPost.data);
    }

    getCampaignsByAccountId(accountId) {
        const account = new AdAccount(accountId);
        account
            .getCampaigns([
                Campaign.Fields.accounId,
                Campaign.Fields.name,
                Campaign.Fields.objective,
                Campaign.Fields.spendCap,
                Campaign.Fields.recommendations,
                Campaign.Fields.startTime,
                Campaign.Fields.endTime,
                Campaign.Fields.effective_status,
                Campaign.Fields.status,
                Campaign.Fields.buying_type
            ])
            .then(campaigns => campaigns);
    }

    createCampaign(accountId, campaign) {
        const account = new AdAccount(accountId);
        return account
            .createCampaign([], {
                [Campaign.Fields.name]: campaign.name,
                [Campaign.Fields.status]: campaign.status === undefined ? Campaign.Status.paused : campaign.status,
                [Campaign.Fields.objective]: Campaign.Objective.post_engagement,
                [Campaign.Fields.spend_cap]: campaign.SpendCap
            })
            .then(createdCampaign => createdCampaign);
    }

    getSavedAudience(accountId, accessToken) {
        return axios.get(`${process.env.FACEBOOK_API_ADS_URL + accountId}/customaudiences?access_token=${accessToken}`).then(savedAudience => savedAudience.data);
    }

    getAdSets(accountId) {
        const account = new AdAccount(accountId);
        account
            .getAdSets([
                AdSet.Fields.account_id,
                AdSet.Fields.campaign_id,
                AdSet.Fields.instagram_actor_id,
                AdSet.Fields.name,
                AdSet.Fields.status,
                AdSet.Fields.optimization_goal,
                AdSet.Fields.promoted_object,
                AdSet.Fields.adset_schedule,
                AdSet.Fields.is_autobid,
                AdSet.Fields.daily_budget,
                AdSet.Fields.bid_amount,
                AdSet.Fields.start_time,
                AdSet.Fields.end_time,
                AdSet.Fields.targeting
            ])
            .then(adSets => adSets);
    }

    getAdSetsByCampaign(accountId, campaignId) {
        const account = new AdAccount(accountId);
        return account
            .getAdSets([
                AdSet.Fields.account_id,
                AdSet.Fields.campaign_id,
                AdSet.Fields.instagram_actor_id,
                AdSet.Fields.name,
                AdSet.Fields.status,
                AdSet.Fields.optimization_goal,
                AdSet.Fields.promoted_object,
                AdSet.Fields.adset_schedule,
                AdSet.Fields.is_autobid,
                AdSet.Fields.daily_budget,
                AdSet.Fields.bid_amount,
                AdSet.Fields.start_time,
                AdSet.Fields.end_time,
                AdSet.Fields.targeting
            ])
            .then(adSets => {
                var campaignAdSet = [];
                adSets.forEach(element => {
                    if (element._data.campaign_id == campaignId) {
                        campaignAdSet.push(element._data);
                    }
                });
                return campaignAdSet;
            });
    }

    createAdSet(accountId, adSet) {
        const account = new AdAccount(accountId);
        return account
            .createAdSet([], {
                [AdSet.Fields.name]: adSet.name,
                [AdSet.Fields.campaign_id]: adSet.campaignId,
                [AdSet.Fields.optimization_goal]: AdSet.OptimizationGoal.post_engagement,
                [AdSet.Fields.billing_event]: AdSet.BillingEvent.impressions,
                [AdSet.Fields.start_time]: adSet.starTime,
                [AdSet.Fields.end_time]: adSet.end_time,
                [AdSet.Fields.daily_budget]: adSet.dailyBudget,
                [AdSet.Fields.status]: adSet.status === undefined ? Campaign.Status.paused : adSet.status,
                [AdSet.Fields.is_autobid]: true,
                [AdSet.Fields.targeting]: {
                    [Targeting.Fields.publisher_platforms]: [
                        //'instagram',
                        'facebook',
                        'audience_network',
                        'messenger'
                    ],
                    [Targeting.Fields.geo_locations]: {
                        [Targeting.Fields.countries]: adSet.countries
                    },
                    age_min: 18, //For Default
                    age_max: 65 //For Default
                }
            })
            .then(response => response);
    }

    getCreatives(accountId) {
        const account = new AdAccount(accountId);
        account
            .getAdCreatives([
                AdCreative.Fields.name,
                AdCreative.Fields.body,
                AdCreative.Fields.thumbnail_url,
                AdCreative.Fields.link_url,
                AdCreative.Fields.instagram_actor_id,
                AdCreative.Fields.instagram_permalink_url,
                AdCreative.Fields.status,
                AdCreative.Fields.object_type,
                AdCreative.Fields.object_url,
                AdCreative.Fields.instagram_story_id,
                AdCreative.Fields.object_story_id,
                AdCreative.Fields.object_story_spec,
                AdCreative.Fields.recommender_settings,
                AdCreative.Fields.platform_customizations,
                AdCreative.Fields.call_to_action_type
            ])
            .then(creatives => creatives);
    }

    createCreative(accountId, creative) {
        const account = new AdAccount(accountId);
        const normalCreative = {
            [AdCreative.Fields.name]: creative.name,
            [AdCreative.Fields.object_story_id]: creative.objectStoryId,
            [AdCreative.Fields.status]: creative.status === undefined ? 'paused' : creative.status
        };
        const instagramCreative = {
            [AdCreative.Fields.name]: creative.name,
            [AdCreative.Fields.object_story_id]: creative.objectStoryId,
            [AdCreative.Fields.instagram_actor_id]: creative.instagram_id,
            [AdCreative.Fields.status]: creative.status === undefined ? 'paused' : creative.status
        };

        return account.createAdCreative([], creative.instagram_id === undefined ? normalCreative : instagramCreative).then(response => response._data.id);
    }

    getInstagramAccounts(accountId) {
        const account = new AdAccount(accountId);
        return account.getInstagramAccounts([]).then(igAccounts => igAccounts);
    }

    validIfInstagramElegible(postId) {
        return axios.get(`${process.env.FACEBOOK_API_ADS_URL}/${postId}?fields=is_instagram_eligible&limit=${limit}&access_token=${accessToken}`).then(response => response);
    }

    getAds(accountId) {
        const account = new AdAccount(accountId);
        account
            .getAds([
                Ad.Fields.id,
                Ad.Fields.name,
                Ad.Fields.bid_amount,
                Ad.Fields.status,
                Ad.Fields.bid_type,
                Ad.Fields.bid_info,
                Ad.Fields.configured_status,
                Ad.Fields.creative,
                Ad.Fields.source_ad,
                Ad.Fields.campaign
            ])
            .then(ads => ads);
    }

    getAdsByAdSetId(accountId, adSetId) {
        const account = new AdAccount(accountId);
        return account
            .getAds([
                Ad.Fields.id,
                Ad.Fields.name,
                Ad.Fields.bid_amount,
                Ad.Fields.status,
                Ad.Fields.bid_type,
                Ad.Fields.bid_info,
                Ad.Fields.configured_status,
                Ad.Fields.creative,
                Ad.Fields.source_ad,
                Ad.Fields.campaign
            ])
            .then(adsResponse => {
                let ads = [];
                adsResponse.forEach(ad => {
                    if (ad.adSetId === adSetId) {
                        ads.push(ad);
                    }
                });

                return ads;
            });
    }

    createAd(accountId, ad) {
        const account = new AdAccount(accountId);
        return account
            .createAd([], {
                [Ad.Fields.name]: ad.name,
                [Ad.Fields.status]: ad.status === undefined ? Campaign.Status.paused : ad.status,
                [Ad.Fields.adset_id]: ad.adSetId,
                [Ad.Fields.creative]: {
                    creative_id: ad.creativeId
                }
            })
            .then(adResponse => adResponse._data.id);
    }

    validateRepeatedSchedulePost(pageId, accessToken, post) {
        this.getSchedulePosts(pageId, accessToken, 10).then(schedulePosts => {
            let postId = '';
            schedulePosts.forEach(schedulePost => {
                if (post.message === schedulePost.message && postId === '') {
                    postId = schedulePost.id;
                    return false;
                }
            });
            return postId;
        });
    }

    getTokenByPageId(pageId) {
        return axios.get(`${process.env.FACEBOOK_API_ADS_URL + pageId}/?fields=access_token&access_token=${process.env.FACEBOOK_API_ADS_TOKEN}`).then(accounts => accounts.data.access_token);
    }
}

module.exports = new FacebookService();
